cmake_minimum_required(VERSION 3.3)
project(Calculate_Distance)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -lm")

set(SOURCE_FILES
#        main.c
        CalculateDistance.c
#        qegrep.c
#        regexTest.c
)
add_executable(Calculate_Distance ${SOURCE_FILES})