#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <math.h>

typedef struct {
    int x;
    int y;
} coord;

int regexMatch(char *source, const char *regexString, size_t maxMatches, size_t maxGroups, char *out[]);

int getCoord(char *string, coord *Coord);

int main(int argc, const char *argv[]) {
    FILE *file = fopen(argv[1], "r");
    char line[1024];
    while (fgets(line, 1024, file)) {
        coord Coord[2];

        int getCoord_result;
        getCoord_result = getCoord(line, Coord);
        if (getCoord_result != 0) {
            return getCoord_result;
        }

        int x = Coord[1].x - Coord[0].x;
        int y = Coord[1].y - Coord[0].y;
        int distance = (int) sqrt(x * x + y * y);
        printf("%d\n", distance);
    }
    return 0;
}

int getCoord(char *string, coord *Coord) {
    char *regexString1 = "-?[0-9]+,\\s-?[0-9]+";
    char *regexString2 = "[-?[0-9]+]*";

    size_t maxMatches = 2;
    size_t maxGroups = 1;

    char *out[2];
    out[0] = malloc(10);
    out[1] = malloc(10);

    int regex_result;
    regex_result = regexMatch(string, regexString1, maxMatches, maxGroups, out);
    if (regex_result != 0) {
        return regex_result;
    }

    char *coordStr[2];
    coordStr[0] = malloc(10);
    coordStr[1] = malloc(10);

    int nOut = sizeof(out) / sizeof(out[0]);
    for (int i = 0; i < nOut; i++) {
        regex_result = regexMatch(out[i], regexString2, maxMatches, maxGroups, coordStr);
        if (regex_result != 0) {
            return regex_result;
        }

        Coord[i].x = atoi(coordStr[0]);
        Coord[i].y = atoi(coordStr[1]);
    }

    return 0;
}

int regexMatch(char *source, const char *regexString, size_t maxMatches, size_t maxGroups, char *out[]) {
    regex_t regexCompiled;
    regmatch_t groupArray[maxGroups];
    char *cursor;

    if (regcomp(&regexCompiled, regexString, REG_EXTENDED)) {
        printf("Could not compile regular expression.\n");
        return 1;
    };

    unsigned int currentMatch;
    cursor = source;
    for (currentMatch = 0; currentMatch < maxMatches; currentMatch++) {
        if (regexec(&regexCompiled, cursor, maxGroups, groupArray, 0))
            break;  // No more matches

        unsigned int currentGroup = 0;
        unsigned int offset = 0;
        for (currentGroup = 0; currentGroup < maxGroups; currentGroup++) {
            if (groupArray[currentGroup].rm_so == (size_t) -1)
                break;  // No more groups

            if (currentGroup == 0)
                offset = (unsigned int) groupArray[currentGroup].rm_eo;

            char cursorCopy[strlen(cursor) + 1];
            strcpy(cursorCopy, cursor);
            cursorCopy[groupArray[currentGroup].rm_eo] = 0;

            strcpy(out[currentMatch], cursorCopy + groupArray[currentGroup].rm_so);
        }
        cursor += offset;
    }

    regfree(&regexCompiled);

    return 0;
}
